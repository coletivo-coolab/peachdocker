FROM jrei/systemd-debian

ARG VERSION="0.6.0"
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apt update && apt install -y wget

RUN wget http://releases.peachcloud.org/peach-web_v$VERSION.tar.gz && tar -xvzf peach-web_v$VERSION.tar.gz
COPY install-docker.sh /usr/src/app/peach-web_v$VERSION

RUN cd ./peach-web_v$VERSION && bash $PWD/install-docker.sh

CMD [ "/sbin/init" ]

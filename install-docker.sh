#!/bin/bash

# PeachCloud install script.
# Installs go-sbot and peach-web.

# echo "[ creating the www-data group if it doesn't already exist ]"
# groupadd www-data
#
# echo "[ adding user to www-data group ]"
# usermod -aG www-data $(whoami)

echo "[ determining host cpu architecture ]"
arch=$(uname -m)

if [[ $arch == 'amd64' ]] || [[ $arch == 'aarch64' ]]
then
  echo "[ installing arm64 binaries ]"
  # Copy the arm64 go-sbot binary into place.
  cp go-sbot_arm64 /usr/bin/go-sbot
  # Copy the arm64 peach-web binary into place.
  cp peach-web_arm64 /usr/bin/peach-web
elif [[ $arch == 'x86_64' ]]
then
  echo "[ installing x86_64 binaries ]"
  # Copy the x86_64 go-sbot binary into place.
  cp go-sbot_x86_64 /usr/bin/go-sbot
  # Copy the x86_64 peach-web binary into place.
  cp peach-web_x86_64 /usr/bin/peach-web
else
  echo "[ error: host architecture not recognised; expected arm64 or x86_64 ]"
  # Exit out of this script.
  exit 1
fi

# echo "[ setting permissions for binaries ]"
# # Allow the home user to execute the binaries.
# chown $(whoami) /usr/bin/go-sbot
# chown $(whoami) /usr/bin/peach-web



echo "[ creating the systemd user directory if it doesn't already exist "]
mkdir -p ~/.config/systemd/user

echo "[ creating the go-ssb directory ]"
mkdir ~/.ssb-go

echo "[ writing the go-sbot configuration file ]"
# The go-sbot will crash if this config file is not in place.
cat > ~/.ssb-go/config.toml << UNIT
# For details about go-sbot configuration, please visit the repo: https://github.com/cryptoscope/ssb
repo = "/home/$(whoami)/.ssb-go"
debugdir = ""
shscap = "1KHLiKZvAvjbY1ziZEHMXawbCEIM6qwjCDm3VYRan/s="
hmac = ""
hops = 2
lis = "127.0.0.1:8008"
wslis = ":8987"
debuglis = "localhost:6078"
localadv = true
localdiscov = true
enable_ebt = true
promisc = true
nounixsock = false
repair = true
UNIT

echo "[ writing the go-sbot unit file ]"
cat > /etc/systemd/system/go-sbot.service << UNIT
[Unit]
Description=go-sbot startup script

[Service]
ExecStart=/usr/bin/go-sbot
Environment="LIBRARIAN_WRITEALL=0"

[Install]
WantedBy=default.target
UNIT

ln -s  /etc/systemd/system/go-sbot.service /etc/systemd/system/default.target.wants/go-sbot.service

# Note that the go-sbot service is not started.
# This is to allow go-sbot to be configured via the peach-web
# interface before the go-sbot is started for the first time.

echo "[ creating directory for peach-web assets ]"
mkdir -p /usr/share/peach-web

echo "[ copying peach-web assets into place ]"
cp -r static /usr/share/peach-web

# echo "[ updating permissions for peach-web assets ]"
# chown -R $(whoami):www-data /usr/share/peach-web

echo "[ writing the peach-web unit file ]"
cat > /etc/systemd/system/peach-web.service << UNIT
[Unit]
Description=peach-web startup script

[Service]
WorkingDirectory=/usr/share/peach-web
Environment="ADDR=0.0.0.0"
Environment="PORT=8000"
Environment="RUST_LOG=info"
ExecStart=/usr/bin/peach-web
Restart=always

[Install]
WantedBy=default.target
UNIT

ln -s  /etc/systemd/system/peach-web.service /etc/systemd/system/default.target.wants/peach-web.service

echo "[ creating peachcloud configuration directory ]"
mkdir -p /var/lib/peachcloud

echo "[ settings permissions for peachcloud configuration directory ]"
# chown -R $(whoami):www-data /var/lib/peachcloud
chmod -R 775 /var/lib/peachcloud
#
# echo "[ --------------------- ]"
# echo "[ installation complete ]"
# echo "[ --------------------- ]"
# echo "peach-web is running on 0.0.0.0:8000"
# echo "the default password is 'peach'"
